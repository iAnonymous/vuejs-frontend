import Vue from 'vue'
import VueRouter from 'vue-router'
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDZ0rHPxn1A-ker-RiNwkMVs4Xj-zJHjJg",
    libraries: "places" 
  }
});

Vue.use(VueRouter)

import App from './App.vue'
import Main from './components/Main.vue'
import Test from './components/Test.vue'
import Find from './components/Find.vue'
import Restaurant from './components/Restaurant.vue'

Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Main },
    { path: '/test', component: Test },
    { path: '/find', component: Find },
    { path: '/restaurants', component: Restaurant }
  ]
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
